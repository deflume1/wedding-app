package com.deflumeriker.wedding;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.widget.Toast;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;

import java.io.File;

/**
 * Wraps the aws-sdk S3 image uploader and associated
 * methods
 *
 * @author deflume1
 * @since September 2018
 */
public class Uploader
{
    public static void upload(final File file, final Context context, final String filename)
    {
        final BasicAWSCredentials credentials = new BasicAWSCredentials("45DNHTJXLR5CNOVKIQAP", "n7fGQEK/R8pzROo64n8x3pTg6J9RePGFfulHsCn5UfY");
        final AmazonS3Client s3 = new AmazonS3Client(credentials);
        s3.setEndpoint("https://deflumeriker.nyc3.digitaloceanspaces.com");
        final TransferUtility transferUtility = TransferUtility.builder().s3Client(s3).context(context).build();

        final TransferObserver observer = transferUtility.upload(
                "", //empty bucket name, included in endpoint
                filename,
                file, //a File object that you want to upload
                CannedAccessControlList.Private
        );

        observer.setTransferListener(new TransferListener()
        {
            @Override
            public void onStateChanged(int id, TransferState state)
            {
                if (TransferState.COMPLETED.equals(observer.getState()))
                {
                    Toast.makeText(context, "Photo upload complete!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onProgressChanged(int id, long bytesCurrent, long bytesTotal)
            {

            }

            @Override
            public void onError(int id, Exception ex)
            {
                Toast.makeText(context, "Photo upload error :(" + ex.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    private static boolean isExternalStorageDocument(final Uri uri)
    {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    private static boolean isDownloadsDocument(final Uri uri)
    {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    private static boolean isMediaDocument(final Uri uri)
    {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    /**
     * Android file handling is garbage, so I took this from
     * https://github.com/awslabs/aws-sdk-android-samples/blob/master/S3TransferUtilitySample/src/com/amazonaws/demo/s3transferutility/UploadActivity.java
     * @param uri - The url you want the path for
     * @return the real filesystem path
     */
    public static String getPath(Uri uri, final Context context, final ContentResolver resolver)
    {
        String path = null;
        String selection = null;
        String[] selectionArgs = null;
        if(DocumentsContract.isDocumentUri(context, uri))
        {
            if (isExternalStorageDocument(uri))
            {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                return Environment.getExternalStorageDirectory() + "/" + split[1];
            }
            else if (isDownloadsDocument(uri))
            {
                final String id = DocumentsContract.getDocumentId(uri);
                uri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));
            }
            else if (isMediaDocument(uri))
            {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];
                if ("image".equals(type))
                {
                    uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                }
                else if ("video".equals(type))
                {
                    uri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                }
                else if ("audio".equals(type))
                {
                    uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }
                selection = "_id=?";
                selectionArgs = new String[] {
                        split[1]
                };
            }
        }

        if ("content".equalsIgnoreCase(uri.getScheme()))
        {
            String[] projection = {
                    MediaStore.Images.Media.DATA
            };
            try (Cursor cursor = resolver.query(uri, projection, selection, selectionArgs, null))
            {
                if(cursor != null)
                {
                    int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                    if (cursor.moveToFirst())
                    {
                        path =  cursor.getString(column_index);
                    }
                }
            }
            catch (Exception e)
            {
                path = null;
            }
        }
        else if ("file".equalsIgnoreCase(uri.getScheme()))
        {
            path = uri.getPath();
        }

        return path;
    }
}
