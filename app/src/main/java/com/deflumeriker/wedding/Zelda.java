package com.deflumeriker.wedding;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Registry;
import com.bumptech.glide.annotation.GlideModule;
import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.module.LibraryGlideModule;
import com.bumptech.glide.integration.okhttp3.OkHttpUrlLoader;

import java.io.InputStream;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;

/**
 * The "Zelda" activity
 *
 * @author deflume1
 * @since September 2018
 */
public class Zelda extends AppCompatActivity
{

    @Override
    protected void onCreate(final Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        final Toolbar myToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);
        setContentView(R.layout.activity_zelda);

        final ImageView image = findViewById(R.id.zelda);

        Glide.with(this).load(getRandomZeldaImageURL()).into(image);

        final TextView myToolbarTextview = findViewById(R.id.toolbarTitle);
        final Typeface typeface = ResourcesCompat.getFont(this, R.font.spacemono_bold);
        myToolbarTextview.setTypeface(typeface);
    }

    /**
     * The onClick for the zelda image
     * @param view - the view
     */
    public void changeImage(final View view)
    {
        if(view instanceof ImageView)
        {
            final ImageView imageView = (ImageView) view;
            Glide.with(this).load(getRandomZeldaImageURL()).into(imageView);
        }
    }

    private static final Random rand = new Random();

    private String getRandomZeldaImageURL()
    {
        final int id = rand.nextInt(36) + 1;
        return "https://cdn.deflumeri.com/zelda/zelda" + id + ".jpg";
    }

    @GlideModule
    public class CustomTimeOutOkHttpGlideModule extends LibraryGlideModule {

        @Override
        public void registerComponents(@NonNull final Context context, @NonNull final Glide glide, @NonNull final Registry registry)
        {
            final OkHttpClient.Builder builder = new OkHttpClient.Builder();

            builder.connectTimeout(500, TimeUnit.MILLISECONDS);
            builder.readTimeout(250, TimeUnit.MILLISECONDS);
            builder.retryOnConnectionFailure(true);

            final OkHttpClient okHttpClient = builder.build();

            registry.replace(GlideUrl.class, InputStream.class,
                    new OkHttpUrlLoader.Factory(okHttpClient));
        }
    }

    /**
     * The onClick for the "back" button
     * @param view - the view
     */
    public void backToMain(final View view)
    {
        finish();
    }
}
