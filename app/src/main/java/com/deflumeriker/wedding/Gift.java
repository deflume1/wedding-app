package com.deflumeriker.wedding;

import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

/**
 * The "Gift" activity
 *
 * @author deflume1
 * @since September 2018
 */
public class Gift extends AppCompatActivity
{

    @Override
    protected void onCreate(final Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        final Toolbar myToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);
        setContentView(R.layout.activity_gift);

        final TextView myToolbarTextview = findViewById(R.id.toolbarTitle);
        final Typeface typeface = ResourcesCompat.getFont(this, R.font.spacemono_bold);
        myToolbarTextview.setTypeface(typeface);
    }

    /**
     * Open the url in the default browser
     * @param url - the url to open
     */
    public void openUrl(final String url)
    {
        final Uri webpage = Uri.parse(url);
        final Intent intent = new Intent(Intent.ACTION_VIEW, webpage);
        if (intent.resolveActivity(getPackageManager()) != null)
        {
            startActivity(intent);
        }
    }

    /**
     * onClick for the Crate & Barrel link
     * @param view - the view
     */
    public void openCrateAndBarrel(final View view)
    {
        openUrl("https://www.crateandbarrel.com/gift-registry/katie-riker-and-chris-deflumeri/r5831587");
    }

    /**
     * onClick for the Zola link
     * @param view - the view
     */
    public void openZola(final View view)
    {
        openUrl("https://www.zola.com/registry/rikerdeflumeri");
    }

    /**
     * onClick for the "back" button
     * @param view - the view
     */
    public void backToMain(final View view)
    {
        finish();
    }
}
