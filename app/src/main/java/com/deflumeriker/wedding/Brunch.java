package com.deflumeriker.wedding;

import android.content.res.Resources;
import android.graphics.Typeface;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.lyft.lyftbutton.LyftButton;
import com.lyft.lyftbutton.RideParams;
import com.lyft.lyftbutton.RideTypeEnum;
import com.lyft.networking.ApiConfig;

/**
 * The Brunch activity.
 *
 * @author deflume1
 * @since September 2018
 */
public class Brunch extends AppCompatActivity implements OnMapReadyCallback
{
    private MapView m_mapView;
    private static final String MAPVIEW_BUNDLE_KEY = "BrunchMapViewBundleKey";

    @Override
    protected void onCreate(final Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        final Toolbar myToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);
        setContentView(R.layout.activity_brunch);

        Bundle mapViewBundle = null;
        if (savedInstanceState != null) {
            mapViewBundle = savedInstanceState.getBundle(MAPVIEW_BUNDLE_KEY);
        }

        m_mapView = findViewById(R.id.brunchMap);
        m_mapView.onCreate(mapViewBundle);
        m_mapView.getMapAsync(this);

        final Resources res = getResources();
        final String clientId = res.getString(R.string.lyft_client_id);
        final String clientToken = res.getString(R.string.lyft_client_token);

        final ApiConfig apiConfig = new ApiConfig.Builder()
                .setClientId(clientId)
                .setClientToken(clientToken)
                .build();

        final LyftButton lyftButton = findViewById(R.id.lyft_button);
        lyftButton.setApiConfig(apiConfig);

        final RideParams.Builder rideParamsBuilder = new RideParams.Builder()
                .setDropoffLocation(42.3735766,-71.0998084)
                .setRideTypeEnum(RideTypeEnum.CLASSIC);

        lyftButton.setRideParams(rideParamsBuilder.build());
        lyftButton.load();

        final TextView myToolbarTextview = findViewById(R.id.toolbarTitle);
        final Typeface typeface = ResourcesCompat.getFont(this, R.font.spacemono_bold);
        myToolbarTextview.setTypeface(typeface);
    }

    @Override
    public void onResume()
    {
        super.onResume();
        m_mapView.onResume();
    }

    @Override
    public void onPause()
    {
        super.onPause();
        m_mapView.onPause();
    }

    @Override
    public void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);
        Bundle mapViewBundle = outState.getBundle(MAPVIEW_BUNDLE_KEY);
        if (mapViewBundle == null)
        {
            mapViewBundle = new Bundle();
            outState.putBundle(MAPVIEW_BUNDLE_KEY, mapViewBundle);
        }

        m_mapView.onSaveInstanceState(mapViewBundle);
    }

    @Override
    public void onLowMemory()
    {
        super.onLowMemory();
        m_mapView.onLowMemory();
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        m_mapView.onDestroy();
    }

    @Override
    public void onMapReady(GoogleMap googleMap)
    {
        googleMap.setMinZoomPreference(12);
        googleMap.getUiSettings().setMapToolbarEnabled(false);
        final LatLng sAndS = new LatLng(42.3735766,-71.0998084);
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sAndS, 12));

        final Marker marker = googleMap.addMarker(new MarkerOptions().position(sAndS)
                .title("S&S Deli")
                .snippet("1334 Cambridge St, Cambridge, MA 02139"));
        marker.showInfoWindow();
    }

    /**
     * onClick for "back"
     * @param view - the view
     */
    public void backToMain(final View view)
    {
        finish();
    }
}
