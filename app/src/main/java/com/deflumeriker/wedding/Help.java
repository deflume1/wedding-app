package com.deflumeriker.wedding;

import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

/**
 * The "Help" activity
 *
 * @author deflume1
 * @since September 2018
 */
public class Help extends AppCompatActivity {

    @Override
    protected void onCreate(final Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        final Toolbar myToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);
        setContentView(R.layout.activity_help);

        final TextView myToolbarTextview = findViewById(R.id.toolbarTitle);
        final Typeface typeface = ResourcesCompat.getFont(this, R.font.spacemono_bold);
        myToolbarTextview.setTypeface(typeface);
    }

    public void textKatie(final View view)
    {
        final Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("smsto:19086017060"));
        intent.putExtra("sms_body", "Hi Katie, I have a wedding question!");
        if (intent.resolveActivity(getPackageManager()) != null)
        {
            startActivity(intent);
        }
    }

    public void textChris(final View view)
    {
        final Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("smsto:15084045449"));
        intent.putExtra("sms_body", "Hi Chris, I have a wedding question!");
        if (intent.resolveActivity(getPackageManager()) != null)
        {
            startActivity(intent);
        }
    }

    public void emailKatie(final View view)
    {
        final Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:katherineriker@gmail.com"));
        intent.putExtra(Intent.EXTRA_SUBJECT, "Wedding Help Needed");
        intent.putExtra(Intent.EXTRA_TEXT, "Hi Katie, I have a wedding question!");
        if (intent.resolveActivity(getPackageManager()) != null)
        {
            startActivity(intent);
        }
    }

    public void emailChris(final View view)
    {
        final Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:chris@deflumeri.com"));
        intent.putExtra(Intent.EXTRA_SUBJECT, "Wedding Help Needed");
        intent.putExtra(Intent.EXTRA_TEXT, "Hi Chris, I have a wedding question!");
        if (intent.resolveActivity(getPackageManager()) != null)
        {
            startActivity(intent);
        }
    }

    public void backToMain(final View view)
    {
        finish();
    }
}
