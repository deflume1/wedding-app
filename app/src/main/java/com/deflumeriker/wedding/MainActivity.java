package com.deflumeriker.wedding;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.ThreadLocalRandom;

/**
 * The main activity.  Handles delegation to other activities, and handles
 * photo uploading
 *
 * @author deflume1
 * @since September 2018
 */
public class MainActivity extends AppCompatActivity implements ActivityCompat.OnRequestPermissionsResultCallback
{

    private String m_currentPhotoPath;
    private static final int REQUEST_TAKE_PHOTO = 1;
    private static final int REQUEST_EXTERNAL_STORAGE = 2;
    private static final int REQUEST_CHOOSE_FILES = 2;

    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    @Override
    protected void onCreate(final Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        final Toolbar myToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);
        setContentView(R.layout.activity_main);

        final TextView myToolbarTextview = findViewById(R.id.toolbarTitle);
        final Typeface typeface = ResourcesCompat.getFont(this, R.font.spacemono_bold);
        myToolbarTextview.setTypeface(typeface);

        final PackageManager packageManager = getPackageManager();
        if(packageManager != null && !packageManager.hasSystemFeature(PackageManager.FEATURE_CAMERA_ANY))
        {
            final Button photosButton = findViewById(R.id.photosButton);
            photosButton.setVisibility(View.GONE);
        }
    }

    /**
     * onClick for the "Ceremony" button
     * @param view - the view
     */
    public void viewCeremony(final View view)
    {
        final Intent intent = new Intent(this, Ceremony.class);
        startActivity(intent);
    }

    /**
     * onClick for the "Reception" button
     * @param view - the view
     */
    public void viewReception(View view)
    {
        final Intent intent = new Intent(this, Reception.class);
        startActivity(intent);
    }

    /**
     * onClick for the "Zelda" button
     * @param view - the view
     */
    public void viewZelda(View view)
    {
        final Intent intent = new Intent(this, Zelda.class);
        startActivity(intent);
    }

    /**
     * onClick for the "Gift" button
     * @param view - the view
     */
    public void viewGift(View view)
    {
        final Intent intent = new Intent(this, Gift.class);
        startActivity(intent);
    }

    /**
     * onClick for the "Brunch" button
     * @param view - the view
     */
    public void viewBrunch(View view)
    {
        final Intent intent = new Intent(this, Brunch.class);
        startActivity(intent);
    }

    /**
     * onClick for the "Help" button
     * @param view - the view
     */
    public void viewHelp(View view)
    {
        final Intent intent = new Intent(this, Help.class);
        startActivity(intent);
    }

    /**
     * onClick for the "Photo" button
     * @param view - the view
     */
    public void takeAPhoto(View view)
    {
        if(verifyStoragePermissions(this))
        {
            startPhotoDialog();
        }
    }

    /**
     * Handle the photo upload activity result
     *
     * @param requestCode - the request code for the activity
     * @param resultCode - the result code for the activity
     * @param data - the data collected from whatever intent was launched
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (requestCode == REQUEST_TAKE_PHOTO && resultCode == RESULT_OK)
        {
            uploadPicFromCamera(m_currentPhotoPath);
        }

        if (requestCode == REQUEST_CHOOSE_FILES && resultCode == RESULT_OK)
        {
            uploadPicFromFile(data.getData());
        }
    }

    /**
     * Upload a pic from a file
     *
     * @param uri - the URI for a file on the device
     */
    private void uploadPicFromFile(final Uri uri)
    {
        final String path = uri != null ? Uploader.getPath(uri, getApplicationContext(), getContentResolver()) : null;
        if(path != null)
        {
            final File file = new File(path);
            // terrible, but w/e.  Presumably we'll always have an extension
            final String filename = getFilename() + file.getName().substring(file.getName().lastIndexOf("."));
            Uploader.upload(new File(path), getApplicationContext(), filename);
        }
        else
        {
            Toast.makeText(this, "Sorry, couldn't upload that file :(", Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Upload a pic from the camera.  If the camera activity completed successfully,
     * m_currentPhotoPath will hold the photo to be uploaded
     */
    private void uploadPicFromCamera(final String currentPhotoPath)
    {
        if(currentPhotoPath != null)
        {
            final File f = new File(currentPhotoPath);
            final Uri contentUri = Uri.fromFile(f);
            final Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
            mediaScanIntent.setData(contentUri);
            this.sendBroadcast(mediaScanIntent);
            Uploader.upload(f, getApplicationContext(), f.getName());
        }
    }

    private static ThreadLocalRandom s_rand = ThreadLocalRandom.current();

    /**
     * Get a temp file name with the datestamp so it'll sort nicely.  Also append a random int
     * so collisions are minimized
     * @return the filename
     */
    private String getFilename()
    {
        return "wedding_" + new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss-SSS", Locale.ENGLISH).format(new Date()) + s_rand.nextInt(100000000);
    }

    /**
     * Create a file where we can store the camera picture
     * @return the File
     * @throws IOException if we don't have permissions
     */
    private File createImageFile() throws IOException
    {
        // Create an image file name
        final String imageFileName = getFilename();
        final File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        final File deflumerikerStorage = new File(storageDir.getAbsolutePath() + "/" + "deflumeriker_wedding");
        deflumerikerStorage.mkdirs();
        final File image = new File(deflumerikerStorage.getAbsolutePath() + "/" + imageFileName + ".jpg");
        image.createNewFile();

        // Save a file: path for use with ACTION_VIEW intents
        m_currentPhotoPath = image.getAbsolutePath();
        return image;
    }

    /**
     * Start the photo dialog.  Delegates the actual activity handling to the right
     * method.
     */
    private void startPhotoDialog()
    {
        final Activity ac = this;
        AlertDialog.Builder myAlertDialog = new AlertDialog.Builder(
                ac);
        myAlertDialog.setTitle("Take Or Upload A Photo");
        myAlertDialog.setMessage("Please, make a decision");

        myAlertDialog.setNegativeButton("Open Gallery",
                new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface arg0, int arg1)
                    {
                        dispatchChooseFilesIntent();
                    }
                });

        myAlertDialog.setPositiveButton("Take Photo",
                new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface arg0, int arg1)
                    {
                        dispatchTakePictureIntent();
                    }
                });
        myAlertDialog.show();
    }

    /**
     * Start the file chooser intent.  Only allows
     * you to choose images.
     */
    private void dispatchChooseFilesIntent()
    {
        final Intent chooseFilesIntent = new Intent(Intent.ACTION_OPEN_DOCUMENT, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        chooseFilesIntent.addCategory(Intent.CATEGORY_OPENABLE);
        chooseFilesIntent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
        chooseFilesIntent.setType("image/*");
        startActivityForResult(
                chooseFilesIntent,
                REQUEST_CHOOSE_FILES);
    }


    /**
     * Start the camera intent.
     */
    private void dispatchTakePictureIntent()
    {
        final Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null)
        {
            // Create the File where the photo should go
            File photoFile;
            try
            {
                photoFile = createImageFile();
            }
            catch (IOException ex)
            {
                Toast.makeText(this, "Can't take photos with this device :(", Toast.LENGTH_SHORT).show();
                return;
            }
            if (photoFile != null)
            {
                Uri photoURI = FileProvider.getUriForFile(this,
                        "com.deflumeriker.wedding.fileprovider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }
        }
    }

    /**
     * Checks if the app has permission to write to device storage
     *
     * If the app does not has permission then the user will be prompted to grant permissions
     *
     * @param activity - the current activity
     */
    private static boolean verifyStoragePermissions(Activity activity)
    {
        // Check if we have write permission
        final int permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
            return false;
        }
        return true;
    }

    /**
     * Handle the permissions request for reading/writing storage
     *
     * @param requestCode - the permissions request code
     * @param permissions - the permissions requests
     * @param grantResults - the results that were granted for permissions
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults)
    {
        if(requestCode == REQUEST_EXTERNAL_STORAGE)
        {
            if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
            {
                startPhotoDialog();
            }
            else
            {
                Toast.makeText(this, "Must grant permission to take photos :(", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
