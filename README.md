DeFlumeriker Wedding
======
**DeFlumeriker Wedding** is the day-of wedding app for the 2018 Wedding of Katie Riker & Chris DeFlumeri

#### Screenshot
![Screenshot Android](https://lh3.googleusercontent.com/C0xLIDjaI41_tazhA9mNmiBF3PRTkwOVzL22OI1F6VTDvd-yzIBlmT_-3tREkKCv1n-M=w2880-h1402)

#### App Stores
<!-- edit this image location -->
[![Get it on Google Play](https://play.google.com/intl/en_us/badges/images/generic/en_badge_web_generic.png)](https://play.google.com/store/apps/details?id=com.deflumeriker.wedding)


## Tests
#### Works on
* Android 5.0 and up

#### Does not work on
* Anything else

## Contributors
Chris DeFlumeri - @deflume1

### Third party libraries
* ![Glide](https://github.com/bumptech/glide)
* ![aws-sdk](https://github.com/aws/aws-sdk-android)
* ![okhttp](https://github.com/square/okhttp)
* ![lyft](https://github.com/lyft/lyft-android-sdk)

## Permissions
* Read/Write External Storage

## License 
Apache 2.0

## Version 
* Version 3.0


## Contact

#### Chris DeFlumeri
* Homepage: https://deflumeri.com
* e-mail: chris@deflumeri.com